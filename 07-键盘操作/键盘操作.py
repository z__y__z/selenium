# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 18:20
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 键盘操作.py
# @Software : PyCharm
from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('http://www.baidu.com')
ele = driver.find_element(By.ID, 'kw')
ele.send_keys('软件')
time.sleep(3)
ele.send_keys(Keys.ENTER)
time.sleep(2)
driver.quit()
# 其它常见按键：
# send_keys(Keys.BACK_SPACE)  回退键
# send_keys(Keys.SPACE) 空格键
# send_keys(Keys.TAB)  Tab
# send_keys(Keys.ESCAPE) ESC
# send_keys(Keys.ENTER)  回车键
# send_keys(Keys.DOWN)  向下键
# send_keys(Keys.UP)  向上键
# send_keys(Keys.LEFT)  向左键
# send_keys(Keys.RIGHT)  向右键
# send_keys(Keys.CONTROL,'a') 全选
# send_keys(Keys.CONTROL,'c') 复制
# send_keys(Keys.CONTROL,'x') 剪切
# send_keys(Keys.CONTROL,'v') 粘贴
# send_Keys(Keys.F1)