# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/7 14:08
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : js_输入.py
# @Software : PyCharm
from selenium import webdriver
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')


#通过js完成输入
text = '手机'
js = f'document.querySelector("#search-input").value="{text}";'
driver.execute_script(js)  #执行js代码