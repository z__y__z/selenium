# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 18:54
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 窗口切换.py
# @Software : PyCharm
from selenium import webdriver
import time
from selenium.webdriver.common.by import By
driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('http://www.baidu.com')
time.sleep(2)
driver.find_element(By.XPATH, '//*[@id="s-top-left"]/a[1]').click()
time.sleep(2)
main = driver.current_window_handle
ss = driver.window_handles
for i in ss:
    driver.switch_to.window(i) #切换窗口
    if driver.current_url == 'http://news.baidu.com/':
        break
driver.find_element(By.XPATH, '//*[@id="pane-news"]/div/ul/li[1]/strong/a').click()
time.sleep(1)
driver.switch_to.window(main)#切换到主窗口 main是变量
driver.find_element(By.ID, 'kw').send_keys('软件')
time.sleep(1)
driver.quit()
# 代码默认在当前窗口查找元素
# driver.current_window_handle    当前窗口的句柄
# driver.window_handles    多个窗口的句柄
# driver.switch_to.window()    切换窗口