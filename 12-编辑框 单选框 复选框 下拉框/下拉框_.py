# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/6 13:56
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 下拉框_.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get(r'D:\pyworkspace\selenium_\12-编辑框 单选框 复选框 下拉框\下拉框.html')

#######################################################
#不用select类，原始做法
#多选
# driver.find_element(By.CSS_SELECTOR, "#multi>option[value='accord']").click()
# time.sleep(1)
# driver.find_element(By.CSS_SELECTOR, "#multi>option[value='bmw']").click()
# time.sleep(1)
# driver.find_element(By.CSS_SELECTOR, "#multi>option[value='audi']").click()
# time.sleep(1)
# driver.find_element(By.CSS_SELECTOR, "#single>option[value='male']").click()
# time.sleep(1)
####################################################################
#导入select类
from selenium.webdriver.support.ui import Select
#获得相应的webdriver对象
select = Select(driver.find_element(By.ID, 'multi'))
#去除所有的选型
select.deselect_all()#全部取消
time.sleep(2)
select.select_by_visible_text('雅阁')#按文本查找
select.select_by_index(3)#从索引0开始
time.sleep(2)
select.select_by_value('bmw')#按value值选择，已经选中，不会再选

#####################################################
#获得相应的webdriver对象
select = Select(driver.find_element(By.ID, 'single'))
select.select_by_visible_text('男')
time.sleep(2)
##################################################
driver.quit()