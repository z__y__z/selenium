# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/6 13:47
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 复选框_.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get(r'D:\pyworkspace\selenium_\12-编辑框 单选框 复选框 下拉框\复选框.html')
time.sleep(1)
aa = driver.find_element(By.CSS_SELECTOR, "input[value='car']")

#判断元素是否选中，选中返回Ture,未选中返回False
ss = aa.is_selected()
print(ss)
print(type(ss))
if ss:
    print('car already selected')
else:
    print('car not selected')
    aa.click()
time.sleep(1)
driver.quit()