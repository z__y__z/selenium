# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/6 13:34
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 编辑框 _.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get(r'D:\pyworkspace\selenium_\12-编辑框 单选框 复选框 下拉框\编辑框.html')
input1 = driver.find_element(By.ID, 'input1')
#通过获取它的value属性来打印它的值
print(input1.get_attribute('value'))
time.sleep(1)
input1.clear()
time.sleep(1)
input1.send_keys('测试')
time.sleep(1)
print(input1.get_attribute('value'))
driver.quit()
