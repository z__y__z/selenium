# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 15:16
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : cookies_.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)

#####################################
#打开登陆页面
driver.get('http://www.shopxo.com/shopxo/index.php?s=/index/user/logininfo.html')

#登录， 取消登录验证码
driver.find_element(By.NAME, 'accounts').send_keys('test01')
driver.find_element(By.NAME, 'pwd').send_keys('123456')
time.sleep(10)
driver.find_element(By.NAME, 'pwd').submit()

#####################################
#获取所有的cookies(字典列表)
print(driver.get_cookies())
print(type(driver.get_cookies()))
################################
dict1 = driver.get_cookie('PHPSESSID')
print(dict1)#获取所有的cookies的name是PHPSESSID
driver.quit()
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php?s=/index/user/logininfo.html')
driver.delete_all_cookies()#删除所有的cookies
######################################
#增加新的的cookie
driver.add_cookie({'name': dict1['name'], 'value': dict1['value']})
driver.refresh()
time.sleep(15)
driver.quit()
#######################################
#第一次手工完成登录，通过代码把cookie内容取出来，关闭浏览器后，
# 服务器对应的session内容不会删除，也不会清空；第二次通过代码，
# 导入之前的cookies内容，与服务器的session建立起关联，绕过登录验证
#driver.get_cookies() 获取所有cookies,返回字典列表
#driver.get_cookie(name)  返回字典中key为name的cookies
#driver.add_cookie(dict) 添加cookie
#driver.delete_cookie(name) 根据name删除指定cookie
#driver.delete_all_cookies() 删除所有cookies