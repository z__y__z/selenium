# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 13:22
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 定位一组元素.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
list1 = driver.find_element(By.ID, 's-top-left').find_elements(By.TAG_NAME, 'a')#返回列表
for ll in list1:
    print(ll.text)
print(list1[2].text)
time.sleep(1)
driver.quit()
#所有查找元素的代码都可以写成 find_elements,返回一个列表，找不到返回一个空列表 默认找第一个