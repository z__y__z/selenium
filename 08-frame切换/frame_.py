# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 18:30
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : frame_.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
#frame是HTML存在嵌套的行为，代码默认只会在主HTML查找元素，不会到子HTML查找
#HTML可以通过frame和iframe嵌套HTML
driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('https://music.163.com/#/discover/toplist')
driver.switch_to.frame('g_iframe')#可填id或name或索引（从0开始）或webelement对象

# driver.switch_to.frame(reference) 一层一层往里面切换
# driver.switch_to.parent_frame() 一层一层往外面切换
# driver.switch_to.default_content() 直接切换到最外面html

a = driver.find_element(By.CLASS_NAME, 'rk ')
print(a.text)
driver.switch_to.default_content()#切换到主的html
s = driver.find_element(By.XPATH, '//*[@id="g_nav2"]/div/ul/li[1]/a/em')
print(s.text)
driver.quit()