# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 16:59
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 显示等待_局部.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
ele = WebDriverWait(driver, 10, 1).until(EC.presence_of_element_located((By.ID, 'kw')))
# #可以设置高频率的周期性查找，直到达到超时时间，适合找一些容易消失的元素；非全局
ele.send_keys('软件')
time.sleep(3)
driver.find_element(By.ID, 'su').click()
driver.quit()