# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 16:53
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 强制等待_局部.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time     #局部等待
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.find_element(By.ID, 'kw').send_keys('软件')
driver.find_element(By.ID, 'su').click()
time.sleep(3)
driver.quit()
