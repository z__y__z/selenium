# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 16:55
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 隐式等待_全局.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
driver = webdriver.Chrome()
driver.implicitly_wait(10)
#全局等待
#等待页面全部加载完去查找元素，找到继续执行，找不到每隔0.5秒轮询查找，直至达到超时时间；全局设定
driver.get('https://www.baidu.com/')
driver.find_element(By.ID, 'kw').send_keys('软件')
driver.find_element(By.ID, 'su').click()
driver.quit()