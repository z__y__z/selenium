# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/7 14:22
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 元素截图-.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
ele = driver.find_element(By.CSS_SELECTOR, '.search-group')#只截图窗口  按元素截图
ele.screenshot(r'd:\截图.png')
driver.quit()