# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/7 14:30
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 窗口截图_.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
###########################################
driver.get_screenshot_as_file(r'd:\1.png') #整个窗口截图
#driver.save_screenshot(r'd:\1.png')#源码再调用get_screenshot_as_file方法
driver.quit()