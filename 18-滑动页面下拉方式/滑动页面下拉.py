# 若要对页面中的内嵌窗口中的多个滚动条进行操作
# 要先定位到该内嵌窗口，在进行滚动条操作
# 使用这个方法：
# （其中，scall是这个div的class的名字
#  如过就一个这种div，直接中括号后面用0就可以
#  如果多个滑动div的class的name一样，
#  需要先找找你需要下拉的div是第几个，如果是第三个，把下面[0],改为[2]）

# js = 'document.getElementsByClassName("scroll")[0].scrollTop=10000'
# 就是这么简单，修改这个元素的scrollTop就可以
# driver.execute_script(js)

# js = "var q=document.getElementById('id').scrollTop=100000"
# driver.execute_script(js)
# time.sleep(3)

# 如果滑动整个大的页面到底部
# 方法一
# driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
# 方法二
# #将滚动条移动到页面的底部
# js="var q=document.documentElement.scrollTop=100000"
# driver.execute_script(js)
# time.sleep(3)

# 将滚动条移动到页面的顶部
# js="var q=document.documentElement.scrollTop=0"
# driver.execute_script(js)
# time.sleep(3)