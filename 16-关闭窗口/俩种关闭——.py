# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/7 14:35
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 俩种关闭——.py
# @Software : PyCharm
from selenium import webdriver

driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.close() #关闭当前窗口
#driver.quit()#关闭所有窗口和退出所有驱动程序
