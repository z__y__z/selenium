# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 18:07
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 鼠标左击.py
# @Software : PyCharm
from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
driver = webdriver.Chrome()
ac = ActionChains(driver)
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('http://www.baidu.com')
ele = driver.find_element(By.CLASS_NAME, 'hot-refresh-text')
time.sleep(1)
ac.click(ele).perform()
time.sleep(2)
driver.quit()