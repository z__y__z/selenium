# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 17:30
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 鼠标悬停.py
# @Software : PyCharm
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains #动作链条
from selenium import webdriver
import time
driver = webdriver.Chrome()
ac = ActionChains(driver) #赋值获取动作链条
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('http://www.baidu.com')
ele = driver.find_element(By.ID, 's-usersetting-top')
ac.move_to_element(ele).perform() #鼠标悬停
time.sleep(3)
driver.quit()