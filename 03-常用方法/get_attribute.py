# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 13:36
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : get_attribute.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
a = driver.find_element(By.ID, 'kw')
print('1-------------------------------')
# 打印该元素的属性值
print(a.get_attribute('class'))
print('2---------------------------')
# 打印元素外部html值（包括当前标签的边界）
print(a.get_attribute('outerHTML'))
print('3---------------------------')
# 打印元素内部html值
print(a.get_attribute('innerHTML'))
time.sleep(2)
driver.quit()