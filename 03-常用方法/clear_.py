# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 13:50
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : clear_.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
a = driver.find_element(By.ID, 'kw')
a.send_keys('软件')
time.sleep(2)
a.clear()
time.sleep(1)
driver.quit()