# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 19:14
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : alert弹窗.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
################################################
##alert警告框有三种：alert,confirm,prompt。都不是HTML的元素，无法定位
#############################################

driver = webdriver.Chrome()
driver.maximize_window()
driver.get(r'D:\pyworkspace\selenium_\11-alert弹窗\alert弹出框.html')
time.sleep(1)
#-------------------------------------------
# driver.find_element(By.ID, 'b1').click()
# time.sleep(1)
# driver.switch_to.alert.accept() #切换点击确认按钮
# time.sleep(1)
# driver.find_element(By.ID, 'other').click()
# time.sleep(1)
##############################################
# driver.find_element(By.ID, 'b1').click()
# alter = driver.switch_to.alert#获取弹出框文本
# print(alter.text)
###############################################
# driver.find_element(By.ID, 'b2').click()
# time.sleep(2)
# driver.switch_to.alert.dismiss()#切换点击取消按钮
# time.sleep(1)
#############################################
driver.find_element(By.ID, 'b3').click()
alert = driver.switch_to.alert#弹出框赋值变量到alert
time.sleep(2)
alert.send_keys('测试')#在弹出框输入测试
time.sleep(2)
alert.accept()#切换点击弹出框确认按钮
time.sleep(1)
driver.find_element(By.ID, 'other').click()
time.sleep(1)
#############################################
driver.quit()
