# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 18:43
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 获取url地址和标题.py
# @Software : PyCharm
from selenium import webdriver
driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)
driver.get('http://www.baidu.com')
print(driver.current_url)#获取当前url地址
print(driver.title)#获取当前的标题
driver.quit()
