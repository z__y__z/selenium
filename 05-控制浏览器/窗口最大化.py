# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 17:17
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 窗口最大化.py
# @Software : PyCharm
from selenium import webdriver
import time
driver = webdriver.Chrome()
driver.get('http://www.baidu.com')
time.sleep(1)
driver.maximize_window()
time.sleep(1)
driver.quit()