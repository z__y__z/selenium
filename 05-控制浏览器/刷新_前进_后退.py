# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 17:20
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 刷新_前进_后退.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.baidu.com')
a = driver.find_element(By.ID, 'kw')
a.send_keys('软件策划')
a.submit()

time.sleep(2)
driver.refresh()
####刷新页面

time.sleep(2)
driver.back()
##等待2秒后退

time.sleep(2)
driver.forward()
##等待2秒后前进

time.sleep(2)
driver.quit()