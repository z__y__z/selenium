# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 17:11
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 获取窗口大小.py
# @Software : PyCharm
from selenium import webdriver
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
print(driver.get_window_size())
#driver.get_window_size() 返回字典，{'width': 1050, 'height': 740}

print(driver.set_window_size(800, 400))
#driver.set_window_size() 宽度和高度设置为超过最大尺寸，也只能显示成最大尺寸

print(driver.get_window_size())
time.sleep(1)
driver.quit()