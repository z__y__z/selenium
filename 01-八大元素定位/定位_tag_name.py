# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 13:02
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 定位_tag_name.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://192.168.10.254:9003/shopxo/')
a = driver.find_element(By.TAG_NAME, 'header')
print(a.text)
time.sleep(3)
driver.quit()