# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 12:55
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 定位_link_text.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('https://www.baidu.com/')
driver.find_element(By.LINK_TEXT, '登录').click()
time.sleep(3)
driver.quit()
