# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 12:49
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 定位_id.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.find_element(By.ID, 'kw').send_keys('软件测试')
time.sleep(1)
driver.quit()

#找不到元素报错：selenium.common.exceptions.NoSuchElementException