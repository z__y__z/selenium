# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 13:38
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_并列选择器.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# 并列选择器 条件连着写
# 此处是先写标签名后写id值
ele = driver.find_element(By.CSS_SELECTOR, 'input#search-input')
ele.send_keys('手机')
time.sleep(2)

driver.quit()



