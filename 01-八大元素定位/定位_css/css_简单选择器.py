# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 11:12
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_简单选择器.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
# 搜索框输入‘手机’，点击搜索
# 搜素框通过id找 以 # 开头
driver.find_element(By.CSS_SELECTOR, '#search-input').send_keys('手机')
time.sleep(2)

# 搜素按钮通过class找  以 . 开头 空格代表具有多种class属性
driver.find_element(By.CSS_SELECTOR, ".submit").click()
time.sleep(2)

# 打印出导航text 通过标签找 以 标签名 开头
a = driver.find_element(By.CSS_SELECTOR, 'header').text
print(a)

driver.quit()