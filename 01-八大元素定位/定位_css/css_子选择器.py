# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 13:31
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_子选择器.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# >子选择器
eles = driver.find_elements(By.CSS_SELECTOR, ".menu-hd>em")
for ele in eles:
    print(ele.text)
time.sleep(1)
driver.quit()