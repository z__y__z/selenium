# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 14:25
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_属性选择器.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# class属性通过属性选择器查找，class属性的值是一个整体（包含空格）

# 点击搜素按钮
# [=] 属性相等
# time.sleep(1)
# ele = driver.find_element(By.CSS_SELECTOR, "input[type='submit']")
# ele.click()
# time.sleep(1)

# [*=] 属性包含
# time.sleep(1)
# ele = driver.find_element(By.CSS_SELECTOR, "input[type*='sub']")
# ele.click()
# time.sleep(1)

# [^=] 属性开头
# time.sleep(1)
# ele = driver.find_element(By.CSS_SELECTOR, "input[type^='sub']")
# ele.click()
# time.sleep(1)

# [$=] 属性结尾
# time.sleep(1)
# ele = driver.find_element(By.CSS_SELECTOR, "input[type$='mit']")
# ele.click()
# time.sleep(1)

# class属性的值是一个整体（包含空格）
time.sleep(1)
ele = driver.find_element(By.CSS_SELECTOR, "input[class='submit am-btn']")
ele.click()
time.sleep(1)

time.sleep(1)
driver.quit()