# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 14:44
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_补充_父亲和后代.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# nth-child() 父亲的第几个孩子
# 1.此处做法是 先以类属性值找到父亲 再以空格找再父亲下的父亲
# 2.再以空格找父亲下的a标签 再数它是的几个孩子
print('---------nth-child() ------------')
ele = driver.find_element(By.CSS_SELECTOR, '.top-nav-left .menu-hd :nth-child(3)')
print(ele.text)
print()

# nth-last-child() 父亲的倒数第几个孩子
# 1.此处做法是 先以类属性值找到父亲 再以空格找再父亲下的父亲
# 2.再以空格找父亲下的a标签 再数它是的父亲下倒数第几个
print('---------nth-last-child() ------------')
ele = driver.find_element(By.CSS_SELECTOR, '.top-nav-left .menu-hd a:nth-last-child(2)')
print(ele.text)
print()

# nth-of-type() 某个标签的第几个
# 1.此处做法是 先以类属性值找到父亲 再以空格找再父亲下的父亲
# 2.再以空格找父亲下的a标签 再数它是a标签第几个
print('---------nth-of-type() ------------')
ele = driver.find_element(By.CSS_SELECTOR, '.top-nav-left .menu-hd a:nth-of-type(2)')
print(ele.text)
print()

# nth-last-of-type() 某个标签的倒数第几个
# 1.此处做法是 先以类属性值找到父亲 再以空格找再父亲下的父亲
# 2.再以空格找父亲下的a标签 再数它是a标签倒数第几个
print('---------nth-last-of-type() ------------')
ele = driver.find_element(By.CSS_SELECTOR, '.top-nav-left .menu-hd a:nth-last-of-type(2)')
print(ele.text)
print()

# not() 不是某个标签的
# 1.此处做法是 先以类属性值找到父亲 再以空格找再父亲下的父亲
# 2.再以空格找父亲下的不是a标签的所有标签
print('---------not() ------------')
eles = driver.find_elements(By.CSS_SELECTOR, '.top-nav-left .menu-hd :not(a)')
for ele in eles:
    print(ele.text)
print()

time.sleep(1)
driver.quit()

# nth-child() 父亲的第几个孩子
# nth-last-child() 父亲的倒数第几个孩子
# nth-of-type() 某个标签的第几个
# nth-last-of-type() 倒着数 某个标签的倒数第几个
# not() 不是某个标签的所有标签



