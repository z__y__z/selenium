# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 13:44
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_后代选择器.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# 后代选择器 以空格
# 此处是先写类属性值  再写类的后代标签名
eles = driver.find_elements(By.CSS_SELECTOR, '.top-nav-left em')
for ele in eles:
    print(ele.text)
time.sleep(2)

driver.quit()