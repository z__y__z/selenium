# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 13:58
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : css_兄弟元素选择器.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# + 后面紧跟着的弟弟
# 一楼 数码办公
# 此处做法 先用id值找到父亲 再以空格找后代 以后代的类属性值查找，再以 +和标签名 找后面紧跟的弟弟
print("----------------+ 后面紧跟着的弟弟------------------------------")
ele = driver.find_element(By.CSS_SELECTOR, '#floor1 .floor-title+p')
print(ele.text)
print()

# ~ 后面所有的p标签弟弟
# 一楼 数码办公
# 此处做法 先用id值找到父亲 再以空格找后代 以后代的类属性值查找，再以 ~和p标签名找后面所有的弟弟
print('------------------~ 后面所有的p标签弟弟-----------------------------')
ele = driver.find_element(By.CSS_SELECTOR, '#floor1 .floor-title~p')
print(ele.text)
print()

# ~ 后面所有的div标签弟弟
# 一楼 数码办公
# 此处做法 先用id值找到父亲 再以空格找后代 以后代的类属性值查找，再以 ~和div标签名找后面所有的弟弟
print('----------------~ 后面所有的div标签弟弟---------------------------')
ele = driver.find_element(By.CSS_SELECTOR, '#floor1 .floor-title~div')
print(ele.text)
print()

# ~ 后面所有的弟弟
# 一楼 数码办公
# 此处做法 先用id值找到父亲 再以空格找后代 以后代的类属性值查找，再以 ~ * 找后面所有的弟弟
print('---------------~ 后面所有的弟弟-----------------------')
eles = driver.find_elements(By.CSS_SELECTOR, '#floor1 .floor-title~*')
for ele in eles:
    print(ele.text)
print()


time.sleep(2)
driver.quit()