# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/5 12:42
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : 定位_class.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
driver.find_element(By.CLASS_NAME, 's_ipt').send_keys('软件测试')#class有空格，代表具有多种属性
time.sleep(1)
driver.quit()