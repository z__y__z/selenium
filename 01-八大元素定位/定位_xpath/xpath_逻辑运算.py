# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 18:29
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_逻辑运算.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
time.sleep(1)

# 搜索框输入手机
# 逻辑运算符 //input[@name='wd' and @id='search-input']
# 此处做法 通过绝对路径找到标签是input的元素 然后查看是否满足name属性值和id属性值 同时满足才可以
driver.find_element(By.XPATH, "//input[@name='wd' and @id='search-input']").send_keys('手机')

time.sleep(1)
driver.quit()