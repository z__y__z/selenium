# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 18:36
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_匹配页面文本.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
time.sleep(1)

# 定位到个人中心，打印页面文本
# 匹配页面文本 //*[text()='个人中心']
# 此处做法 通过相对路径 匹配任意标签下的文本与条件相同的元素
ele = driver.find_element(By.XPATH, "//*[text()='个人中心']")
print(ele.text)

time.sleep(1)
driver.quit()