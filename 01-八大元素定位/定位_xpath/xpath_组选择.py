# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 17:47
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_组选择.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
time.sleep(1)
# 定位到搜索框和搜索按钮，打印html
# 此处做法 通过通配符* 匹配俩组任意标签内id满足其一相等的元素 无论满足那一组都可以筛选出来
eles = driver.find_elements(By.XPATH, "//*[@id='search-input']|//*[@id='ai-topsearch']")
for ele in eles:
    print(ele.get_attribute("outerHTML"))

time.sleep(1)
driver.quit()