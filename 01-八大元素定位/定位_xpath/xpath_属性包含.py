# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 17:29
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_属性包含.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)

# 定位到元素框，打印html(包括边框)
# contains包含  //input[contains(@id,'search')]
# 此处做法是 用相对路径找到div标签下的包含class属性值相等的元素
ele = driver.find_element(By.XPATH, "//div[contains(@class,'search-group')]")
print(ele.get_attribute("outerHTML"))


time.sleep(1)
driver.quit()