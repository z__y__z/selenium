# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 17:39
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_通配符.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
time.sleep(1)
# 搜索输入手机
# 此处做法 通过通配符* 匹配任意标签内id相等的元素
driver.find_element(By.XPATH, "//*[@id='search-input']").send_keys('手机')

time.sleep(1)
driver.quit()