# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 15:35
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_属性选择.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
# //开头，以根作为起点，找任意后代标签；/开头，以根作为起点，一层一层查找
# 搜素输入框
# 此处做法是 通过相对路径找到input标签里有相等的id的值
time.sleep(1)
driver.find_element(By.XPATH, "//input[@id='search-input']").send_keys('手机')
time.sleep(1)

# 刷新 清除输入框内容
driver.refresh()
time.sleep(1)

# 多重查找
# 此处做法是 通过相对路径找到input标签里有相等的id的值与name值
driver.find_element(By.XPATH, "//input[@name='wd'][@id='search-input']").send_keys('软件')
time.sleep(1)

driver.quit()