# _*_ coding: UTF-8 _*_
# @Time     : 2022/1/9 17:59
# @Author   : zyz
# @Site     : 1012081978@qq.com
# @File     : xpath_补充.py
# @Software : PyCharm
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://www.shopxo.com/shopxo/index.php')
driver.implicitly_wait(10)
time.sleep(1)
# []前面是标签名，是这个类型的第几个；[]前面是*，是父亲的第几个后代

# 定位到搜索按钮
# //input[2]  input类型的第二个
# 此处做法 通过相对路径找到input下的第二个元素
ele = driver.find_element(By.XPATH, '//input[2]')
print(ele.get_attribute("outerHTML"))

# 定位到左上角的登录链接
# //*[@class='top-nav-left']//*[3] 父亲第三个后代
# 此处做法 通过相对路径找到任意满足class属性值的标签即父亲 然后找父亲任意标签的第三个孩子
ele = driver.find_element(By.XPATH, "//*[@class='top-nav-left']//*[3]")
print(ele.get_attribute("outerHTML"))

# 定位到左上角的注册链接
# last() 最后一个
# 此处做法 通过相对路径找到任意满足class属性值的标签即父亲 然后找父亲a标签的最后一个孩子
ele = driver.find_element(By.XPATH, "//*[@class='top-nav-left']//a[last()]")
print(ele.get_attribute("outerHTML"))

# 定位到左上角的注册链接
# position() 第几个元素，支持>,>=,<,<=等比较运算
# 此处做法 通过相对路径找到任意满足class属性值的标签即父亲 然后找父亲a标签选择等于last()的孩子
ele = driver.find_element(By.XPATH, "//*[@class='top-nav-left']//a[position()=last()]")
print(ele.get_attribute("outerHTML"))

# 定位到左上角的注册链接
# position() 第几个元素，支持>,>=,<,<=等比较运算
# 此处做法 通过相对路径找到任意满足class属性值的标签即父亲 然后找父亲a标签选择大于last()-1的孩子
ele = driver.find_element(By.XPATH, "//*[@class='top-nav-left']//a[position()>last()-1]")
print(ele.get_attribute("outerHTML"))


# 补充
# //*[@class='menu-hd']/a[1]/preceding-sibling::em    em的哥哥/姐姐
# //*[@class='menu-hd']/a[1]/following-sibling::a    a的弟弟/妹妹
# ..上一级   .当前级
# /前后是直接子元素关系（绝对路径）
# //前后不是直接子元素关系，后代关系（相对路径）
# //开头，以根作为起点，找任意后代标签；/开头，以根作为起点，一层一层查找

time.sleep(1)
driver.quit()